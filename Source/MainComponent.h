/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component, public MidiInputCallback, public Button::Listener, public Slider::Listener
{
public:
    //==============================================================================
    MainComponent();
    ~MainComponent();

    void resized() override;
    
    void handleIncomingMidiMessage(MidiInput*, const MidiMessage& message) override
    {
        String midiText;
        
        if (message.isNoteOnOrOff())
        {
            midiText << "NoteOn: Channel " <<  message.getChannel();
            midiText << " Number " <<  message.getNoteNumber();
            midiText << " Velocity " <<  message.getVelocity();
        }
        else if (message.isAftertouch())
        {
            midiText << "Aftertouch: Channel " << message.getChannel();
            midiText << " Number " << message.getNoteNumber();
            midiText << " Value " << message.getAfterTouchValue();
        }
        else if (message.isPitchWheel())
        {
            midiText << "Pitch Wheel: Channel " <<  message.getChannel();
            midiText << " Value " <<  message.getPitchWheelValue();
        }
        else if (message.isController())
        {
            midiText << "Control: Channel " << message.getChannel();
            midiText << " Number " << message.getControllerNumber();
            midiText << " Value " << message.getControllerValue();
        }
        else if (message.isProgramChange())
        {
            midiText << "Program: Channel " << message.getChannel();
            midiText << " Value " << message.getProgramChangeNumber();
        }
        DBG (midiText);
        midiLabel.getTextValue() = midiText;
        
        audioDeviceManager.getDefaultMidiOutput()->sendMessageNow(message);
    }
    
    void buttonClicked (Button* button) override;
    
    void sliderValueChanged (Slider* slider) override;


private:
    //==============================================================================
    AudioDeviceManager audioDeviceManager;
    Label midiLabel;
    ComboBox midiType;
    Slider midiChannel;
    Slider midiNumber;
    Slider midiVelocity;
    TextButton midiSend;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
