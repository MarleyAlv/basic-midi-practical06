/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (900, 800);
    audioDeviceManager.setMidiInputEnabled ("Virtual Axiom\n", true);
    audioDeviceManager.addMidiInputCallback(String::empty, this);
    addAndMakeVisible(midiLabel);
    midiLabel.setText("label", dontSendNotification);
    
    audioDeviceManager.setDefaultMidiOutput ("SimpleSynth virtual input");
    
    midiType.addItem ("Note", 1);
    midiType.addItem ("Aftertouch", 2);
    midiType.addItem ("Pitch Bend", 3);
    midiType.addItem ("Control", 4);
    midiType.addItem ("Program", 5);
    
    midiChannel.setSliderStyle (Slider::IncDecButtons);
    midiChannel.setRange(0, 15);
    midiNumber.setSliderStyle (Slider::IncDecButtons);
    midiNumber.setRange(0, 127);
    midiVelocity.setSliderStyle (Slider::IncDecButtons);
    midiVelocity.setRange(0, 127);
    
    midiSend.setButtonText ("Send");
    
    addAndMakeVisible (midiType);
    
    addAndMakeVisible (midiChannel);
    midiChannel.addListener (this);
    addAndMakeVisible (midiNumber);
    midiNumber.addListener (this);
    addAndMakeVisible (midiVelocity);
    midiVelocity.addListener (this);
    
    addAndMakeVisible (midiSend);
    midiSend.addListener (this);
}

MainComponent::~MainComponent()
{
    audioDeviceManager.removeMidiInputCallback(String::empty, this);
}

void MainComponent::resized()
{
    midiLabel.setBounds(22, 10, getWidth()-20, 70);
    midiType.setBounds (22, 90, getWidth()/6, 40);
    midiChannel.setBounds (44+(getWidth()/6), 90, getWidth()/6, 40);
    midiNumber.setBounds (66+2*(getWidth()/6), 90, getWidth()/6, 40);
    midiVelocity.setBounds (88+3*(getWidth()/6), 90, getWidth()/6, 40);
    midiSend.setBounds (110+4*(getWidth()/6), 90, getWidth()/6, 40);
}
void MainComponent::buttonClicked (Button* button)
{
    DBG ("Midi sent\n");
}
void MainComponent::sliderValueChanged (Slider* slider)
{
    if (slider == &midiChannel)
    {
        DBG ("slider value: " << midiChannel.getValue());
    }
    if (slider == &midiNumber)
    {
        DBG ("slider value: " << midiNumber.getValue());
    }
    if (slider == &midiVelocity)
    {
        DBG ("slider value: " << midiVelocity.getValue());
    }
}